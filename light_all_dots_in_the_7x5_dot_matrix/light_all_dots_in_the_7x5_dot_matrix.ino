// read README.md for pins_x/y[]
const byte X = 5;
const byte Y = 7;
const byte pins_x[] = {16, 27, 17, 18, 25};
const byte pins_y[] = {28, 24, 21, 26, 20, 15, 19};

void setup()
{
	// first set ground on Y axis
	for (byte j = 0; j < Y; j++) {
		pinMode(pins_y[j], OUTPUT);
		digitalWrite(pins_y[j], LOW);
	}
	// then light the X axis
	for (byte i = 0; i < X; i++) {
		pinMode(pins_x[i], OUTPUT);
		digitalWrite(pins_x[i], HIGH);
	}
}

void loop()
{
	
}

