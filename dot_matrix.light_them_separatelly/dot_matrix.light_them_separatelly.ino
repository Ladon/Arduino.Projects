const byte dimX = 5;
const byte dimY = 7;
const byte pinsX[] = {16, 27, 17, 18, 25};
const byte pinsY[] = {28, 24, 21, 26, 20, 15, 19};

void setup()
{
	for (byte i = 0; i < dimX; i++) {
		pinMode(pinsX[i], OUTPUT);
		digitalWrite(pinsX[i], LOW);
	}
	for (byte j = 0; j < dimY; j++) {
		pinMode(pinsY[j], OUTPUT);
		digitalWrite(pinsY[j], HIGH);
	}
}

void loop()
{
	for (byte i = 0; i < dimX; i++) {
		for (byte j = 0; j < dimY; j++) {
			digitalWrite(pinsX[i], HIGH);
			digitalWrite(pinsY[j], LOW);
			delay(1300);
			digitalWrite(pinsX[i], LOW);
			digitalWrite(pinsY[j], HIGH);
		}
	}
}

