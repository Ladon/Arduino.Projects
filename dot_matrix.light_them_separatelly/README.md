It is a TA20-11SURKWA 5x7 dot-matrix that i have and a fireduino in 3.3V. My father connected some pins to resistors, and as such, i'm using the digital I/O as ground and on/off.


for the following table, the LED's diodes point to the bottom left, like so ::

      /
    |/_
    /

fireduino cannot use analog pins as digital I/O ::

matrix (fireduino) correspondence ::
		13(16)  3(27)  4,11(17)  10(18)  6(25)
9(19)    
14(15)
8(20)
12,5(26)
1(21)
7(24)
2(28)

i'll write the correspondence as a c++ matrix, so that i can easily copy it to code :

const byte pinsX[] = {16, 27, 17, 18, 25};
const byte pinsY[] = {28, 24, 21, 26, 20, 15, 19};